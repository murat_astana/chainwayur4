from peewee import *
from DB.BaseModel import BaseModel
from playhouse.shortcuts import model_to_dict

class ConfigModel(BaseModel):
    device_ip = CharField(null=True)
    device_port = IntegerField(default=8888)

    server_ip = CharField(null=True)
    server_token = CharField(null=True)

    power_dbi = IntegerField(null=True)
    region = CharField(null=True)

    ant_frequency_1 = IntegerField(default=0)
    ant_frequency_2 = IntegerField(default=0)
    ant_frequency_3 = IntegerField(default=0)
    ant_frequency_4 = IntegerField(default=0)

    ant_enabled_1 = IntegerField(default=True)
    ant_enabled_2 = IntegerField(default=True)
    ant_enabled_3 = IntegerField(default=True)
    ant_enabled_4 = IntegerField(default=True)


class ConfigRepo:
    def __init__(self):
        self.__item = None
        self.__check_or_create()

    def __check_or_create(self):
        self.__item = ConfigModel.get_or_none()
        if self.__item is None:
            ConfigModel.create()
            self.__item = ConfigModel.get()

    def get_all(self):
        return model_to_dict(self.__item)

    def get(self, attr: str):
        dict_attrs = model_to_dict(self.__item)
        if attr not in dict_attrs:
            return False

        return dict_attrs[attr]

    def update(self, attr: str, value):
        dict_attrs = model_to_dict(self.__item)
        if attr not in dict_attrs:
            return False

        setattr(self.__item, attr, value)

        self.__item.save()

        return model_to_dict(self.__item)


