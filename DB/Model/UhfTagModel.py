from peewee import *
from playhouse.shortcuts import model_to_dict

from DB.BaseModel import BaseModel
import time


class UhfTagModel(BaseModel):
    epc = CharField(null=True)
    tid = CharField(null=True)
    rssi = FloatField(null=True)
    ant_num = SmallIntegerField(null=True)
    tag_date = DateField(null=True)
    tag_time = TimeField(null=True)


class UhfTagPero:
    def __init__(self):
        pass

    def add(self, epc: str, tid: str, rssi: float, ant_num: int):
        tag_date = time.strftime("%Y-%m-%d")
        tag_time = time.strftime("%H-%M-%S")

        tag = UhfTagModel.create(epc=epc, tid=tid, rssi=rssi, ant_num=ant_num, tag_date=tag_date, tag_time=tag_time)

        return self.__obj_to_map(tag)

    def __obj_to_map(self, tag):
        return {
            "epc": tag.epc,
            "tid": tag.tid,
            "rssi": tag.rssi,
            "ant_num": tag.ant_num,
            "tag_date": tag.tag_date,
            "tag_time": tag.tag_time
        }
