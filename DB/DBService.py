from peewee import *
from DB.Model.ConfigModel import *
from DB.Model.UhfTagModel import *


class DBService:
    db = SqliteDatabase('database.db')
    config = None

    def __init__(self):
        self.db.connect()
        self.db.create_tables([ConfigModel, UhfTagModel])
        self.config = ConfigRepo()
        self.uhf_tag = UhfTagPero()
