from peewee import *
import datetime

db = SqliteDatabase('def_db.db')


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    username = CharField()


class Tweet(BaseModel):
    user = ForeignKeyField(User, backref='tweets')
    message = TextField()
    created_date = DateTimeField(default=datetime.datetime.now)
    is_published = BooleanField(default=True)


db.connect()


asdasd = User.get(User.username == 'charlie')

print(asdasd.username)
