import sys
from PyQt5 import QtCore, QtWidgets

from DB.DBService import DBService
from GUI.MainController import GUIController
from Services.RFID.RFIDCommander import RFIDCommander


def main():
    app = QtWidgets.QApplication(sys.argv)

    # init db
    db = DBService()

    # init uhf commander service
    uhf = RFIDCommander('192.168.1.6', 8888)

    # init gui controller
    controller = GUIController(uhf, db)
    controller.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
