import sys
from PyQt5 import QtWidgets

from DB.DBService import DBService
from GUI.LoginController import LoginController


def main():
    app = QtWidgets.QApplication(sys.argv)

    # init db
    db = DBService()

    # init gui controller
    controller = LoginController(db)
    controller.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
