from DB.DBService import DBService
from GUI.MainController import MainController
from GUI.Screens.LoginScreen import LoginScreen


class LoginController:
    def __init__(self, db: DBService):
        self.main_controller = None
        self.__uhf = None
        self.__db = db
        self.login_screen = None

    def show(self):
        self.login_screen = LoginScreen(self.__db)
        self.login_screen.switch_window.connect(self.enter)
        self.login_screen.show()

    def enter(self):
        self.__uhf = self.login_screen.uhf
        self.login_screen.close()

        self.main_controller = MainController(self.__uhf, self.__db)
        self.main_controller.show()
