from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator, QIntValidator
from PyQt5.QtWidgets import QPushButton, QLineEdit


class ShowFieldElement:
    def __init__(self, context, field_value: str):
        self.__context = context
        self.__field_value = field_value

        self.__init_widget()

    def __init_widget(self):
        lineEdit = QLineEdit()
        lineEdit.setEnabled(False)
        lineEdit.setText(self.__field_value)

        self.__widget = lineEdit

    @property
    def widget(self):
        return self.__widget
