from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator, QIntValidator
from PyQt5.QtWidgets import QPushButton, QLineEdit


class InputPortElement:
    def __init__(self, context):
        self.__context = context

        self.__init_widget()

    def __init_widget(self):
        onlyInt = QIntValidator()
        onlyInt.setRange(1, 9999)
        lineEdit = QLineEdit()
        lineEdit.setValidator(onlyInt)

        self.__widget = lineEdit

    @property
    def widget(self):
        return self.__widget
