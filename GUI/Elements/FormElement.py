from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtWidgets import QPushButton, QLineEdit, QGroupBox, QFormLayout, QLabel


class FormElement:
    def __init__(self, context, title: str):
        self.__context = context
        self.__title = title

        self.__init_widget()

    def add_row(self, title: str, widget):
        self.__layout.addRow(QLabel(title))
        self.__layout.addRow(widget)

    def add_button(self, widget):
        self.__layout.addRow(widget)

    def __init_widget(self):
        self.__box_widget = QGroupBox(self.__title)
        self.__layout = QFormLayout()

        self.__box_widget.setLayout(self.__layout)

    @property
    def widget(self):
        return self.__box_widget
