from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QGridLayout, QWidget, QTableWidget, QTableWidgetItem
from PyQt5.QtCore import QSize, Qt


class ShowTableElement:
    def __init__(self, context, columns_titles: list):
        self.__row_count = 1
        self.__column_count = len(columns_titles)
        self.__columns_titles = columns_titles
        self.__context = context

        self.__init_table()

    def __init_table(self):
        self.__table = QTableWidget(self.__context)
        self.__table.setColumnCount(self.__column_count)
        self.__table.verticalHeader().hide()

        self.__table.resizeColumnsToContents()
        self.__table.resizeRowsToContents()

        self.__init_columns()
        self.__init_def_row()

        self.__header = self.__table.horizontalHeader()
        self.__header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)

        self.__table.verticalHeader().hide()

        self.__table.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.__table.setFocusPolicy(Qt.NoFocus)

    def __init_columns(self):
        self.__table.setHorizontalHeaderLabels(self.__columns_titles)

        for counter, _ in enumerate(self.__columns_titles):
            self.__table.horizontalHeaderItem(counter).setTextAlignment(Qt.AlignHCenter)

    def __init_def_row(self):
        pass

    def set_cell(self, row_num: int, column_num: int, cell_value: str):
        if row_num == self.__table.rowCount():
            self.__table.setRowCount((row_num + 1))

        self.__table.setItem(row_num, column_num, QTableWidgetItem(cell_value))

    def get_cell(self, row_num: int, column_num: int):
        return self.__table.item(row_num, column_num).text()

    def set_stretch_for_column(self, column_num: int):
        self.__header.setSectionResizeMode(column_num, QtWidgets.QHeaderView.Stretch)


    @property
    def widget(self):
        return self.__table
