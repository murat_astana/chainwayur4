from PyQt5.QtWidgets import QPushButton


class ButtonElement:
    def __init__(self, context, title: str = 'Сохранить'):
        self.__context = context
        self.__title = title

        self.__init_widget()

    def __init_widget(self):
        self.__widget = QPushButton(self.__title, self.__context)


    @property
    def widget(self):
        return self.__widget
