from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator, QIntValidator
from PyQt5.QtWidgets import QPushButton, QLineEdit


class InputTextElement:
    def __init__(self, context):
        self.__context = context

        self.__init_widget()

    def __init_widget(self):
        lineEdit = QLineEdit()

        self.__widget = lineEdit

    @property
    def widget(self):
        return self.__widget
