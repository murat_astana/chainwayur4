from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator, QIntValidator
from PyQt5.QtWidgets import QPushButton, QLineEdit, QComboBox


class InputComboElement:
    def __init__(self, context, items: list):
        self.__context = context
        self.__items = items

        self.__init_widget()

    def __init_widget(self):
        degreeComboBox = QComboBox()
        degreeComboBox.addItems(self.__items)

        self.__widget = degreeComboBox

    @property
    def widget(self):
        return self.__widget
