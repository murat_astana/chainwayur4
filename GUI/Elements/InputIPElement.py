from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtWidgets import QPushButton, QLineEdit


class InputIPElement:
    def __init__(self, context):
        self.__context = context

        self.__init_widget()

    def __init_widget(self):
        ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])"  # Part of the regular expression
        # Regulare expression
        ipRegex = QRegExp("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$")
        ipValidator = QRegExpValidator(ipRegex, self.__context)

        lineEdit = QLineEdit()
        lineEdit.setValidator(ipValidator)
        lineEdit.setPlaceholderText("192.168.1.1")

        self.__widget = lineEdit

    @property
    def widget(self):
        return self.__widget


