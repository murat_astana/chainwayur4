from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel

from GUI.Elements.FormElement import FormElement
from GUI.Elements.ShowFieldElement import ShowFieldElement
from Services.RFID.RFIDCommander import CommandNameType
from Services.ServerRequest import ServerRequestService


class InfoScreen(QWidget):
    __main_layout = None
    __table = None
    __uhf = None

    def __init__(self, uhf, server_api: ServerRequestService):
        super(InfoScreen, self).__init__()
        self.__init_layout()
        self.__uhf = uhf
        self.__server_api = server_api

    def __init_layout(self):
        self.__main_layout = QVBoxLayout()

        self.__hardware_widget = ShowFieldElement(self, 'xxxx').widget
        self.__firmware_widget = ShowFieldElement(self, 'xxxx').widget
        self.__temp_widget = ShowFieldElement(self, 'xxxx').widget
        self.__region_widget = ShowFieldElement(self, 'xxxx').widget

        form_widget = FormElement(self, 'Информация')
        form_widget.add_row('Версия аппаратного обеспечения', self.__hardware_widget)
        form_widget.add_row('Версия прошивки', self.__firmware_widget)
        form_widget.add_row('Температура устройства', self.__temp_widget)
        form_widget.add_row('Регион', self.__region_widget)

        self.__main_layout.addWidget(form_widget.widget)

        self.setLayout(self.__main_layout)


    def load_data(self):
        print('load_data from LogScreen:')
        self.__data = self.__get_device_data()
        print(self.__data)
        self.__server_api.save_info(self.__data)

        for attr_key in self.__data.keys():
            if attr_key == 'hardware':
                self.__hardware_widget.setText(self.__data[attr_key])
            if attr_key == 'software':
                self.__firmware_widget.setText(self.__data[attr_key])
            if attr_key == 'temp':
                self.__temp_widget.setText(str(self.__data[attr_key]))
            if attr_key == 'region':
                self.__region_widget.setText(self.__data[attr_key])

    def __get_device_data(self):
        data = {'hardware': self.__uhf.call_command(CommandNameType.GetHardwareVersion),
                'software': self.__uhf.call_command(CommandNameType.GetSoftwareVersion),
                'device_id': self.__uhf.call_command(CommandNameType.GetDeviceID),
                'temp': self.__uhf.call_command(CommandNameType.GetTemperature),
                'region': self.__uhf.call_command(CommandNameType.GetRegion)}

        return data
