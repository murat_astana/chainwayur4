from PyQt5.QtWidgets import QWidget,  QGridLayout

from DB.DBService import DBService
from GUI.Screens.ConfigScreen.AntFrequencyForm import AntFrequencyForm
from GUI.Screens.ConfigScreen.PowerForm import PowerForm
from GUI.Screens.ConfigScreen.ServerForm import ServerForm
from Services.Main.Utils import call_error_dialog, call_info_dialog
from Services.RFID.RFIDCommander import RFIDCommander, CommandNameType
from Services.ServerRequest import ServerRequestService


class ConfigScreen(QWidget):
    __main_layout = None
    __table = None
    __uhf = None
    __db = None

    def __init__(self, uhf: RFIDCommander, db: DBService, server_api: ServerRequestService):
        super(ConfigScreen, self).__init__()
        self.__data = None
        self.__uhf = uhf
        self.__db = db
        self.__server_api = server_api

        self.__server_form = ServerForm(self, self.__server_api)
        self.__power_form = PowerForm(self)
        self.__ant_frequency_1_form = AntFrequencyForm(self, '1')
        self.__ant_frequency_2_form = AntFrequencyForm(self, '2')
        self.__ant_frequency_3_form = AntFrequencyForm(self, '3')
        self.__ant_frequency_4_form = AntFrequencyForm(self, '4')

        self.__init_layout()

    def __init_layout(self):
        self.__main_layout = QGridLayout()
        self.__main_layout.setRowStretch(0 | 1 | 2 | 3, 50)

        self.__main_layout.addWidget(self.__power_form.widget, 0, 0)
        self.__main_layout.addWidget(self.__server_form.widget, 0, 1)

        self.__main_layout.addWidget(self.__ant_frequency_1_form.widget, 1, 0)
        self.__main_layout.addWidget(self.__ant_frequency_2_form.widget, 1, 1)

        self.__main_layout.addWidget(self.__ant_frequency_3_form.widget, 2, 0)
        self.__main_layout.addWidget(self.__ant_frequency_4_form.widget, 2, 1)

        self.setLayout(self.__main_layout)

    def save_data(self, data: dict):
        print(data)
        for attr in data.keys():
            value = data[attr]
            if attr == 'power_dbi':
                r = self.__uhf.call_command(CommandNameType.SetPower, [int(value)])
                if not r:
                    call_error_dialog('Возникла ошибка при сохранении. Попробуйте позже')
                    continue
            if attr == 'ant_frequency_1':
                r = self.__uhf.call_command(CommandNameType.SetANTWorkTime, [1, int(value)])
                if not r:
                    call_error_dialog('Возникла ошибка при сохранении. Попробуйте позже')
                    continue
            if attr == 'ant_frequency_2':
                r = self.__uhf.call_command(CommandNameType.SetANTWorkTime, [2, int(value)])
                if not r:
                    call_error_dialog('Возникла ошибка при сохранении. Попробуйте позже')
                    continue
            if attr == 'ant_frequency_3':
                r = self.__uhf.call_command(CommandNameType.SetANTWorkTime, [3, int(value)])
                if not r:
                    call_error_dialog('Возникла ошибка при сохранении. Попробуйте позже')
                    continue
            if attr == 'ant_frequency_4':
                r = self.__uhf.call_command(CommandNameType.SetANTWorkTime, [4, int(value)])
                if not r:
                    call_error_dialog('Возникла ошибка при сохранении. Попробуйте позже')
                    continue

            self.__db.config.update(attr, value)

        call_info_dialog('Данные успешно сохранены')

        self.__data = self.__db.config.get_all()
        self.__server_api.save_config(self.__data)

    def load_data(self):
        print('Config screen. loaded data:')
        self.__data = self.__get_data()
        print(self.__data)

        for attr_key in self.__data.keys():
            if attr_key == 'server_ip':
                self.__server_form.server_ip_widget.setText(self.__data[attr_key])
            if attr_key == 'server_token':
                self.__server_form.server_token_widget.setText(self.__data[attr_key])
            if attr_key == 'power_dbi':
                self.__power_form.power_widget.setCurrentText(str(self.__data[attr_key]))
            if attr_key == 'ant_frequency_1':
                self.__ant_frequency_1_form.frequency_widget.setText(str(self.__data[attr_key]))
            if attr_key == 'ant_frequency_2':
                self.__ant_frequency_2_form.frequency_widget.setText(str(self.__data[attr_key]))
            if attr_key == 'ant_frequency_3':
                self.__ant_frequency_3_form.frequency_widget.setText(str(self.__data[attr_key]))
            if attr_key == 'ant_frequency_4':
                self.__ant_frequency_4_form.frequency_widget.setText(str(self.__data[attr_key]))

    def __get_data(self):
        device_data = self.__get_device_data()
        for attr in device_data.keys():
            value = device_data[attr]
            self.__db.config.update(attr, value)
        data = self.__db.config.get_all()

        return data

    def __get_device_data(self):
        data = {'power_dbi': self.__uhf.call_command(CommandNameType.GetPower),
                'region': self.__uhf.call_command(CommandNameType.GetRegion),
                'ant_frequency_1': self.__uhf.call_command(CommandNameType.GetANTWorkTime, [1]),
                'ant_frequency_2': self.__uhf.call_command(CommandNameType.GetANTWorkTime, [2]),
                'ant_frequency_3': self.__uhf.call_command(CommandNameType.GetANTWorkTime, [3]),
                'ant_frequency_4': self.__uhf.call_command(CommandNameType.GetANTWorkTime, [4]),
                'ant_enabled_1': True,
                'ant_enabled_2': True,
                'ant_enabled_3': True,
                'ant_enabled_4': True}

        return data
