from PyQt5.QtWidgets import QMessageBox

from GUI.Elements.ButtonElement import ButtonElement
from GUI.Elements.FormElement import FormElement
from GUI.Elements.InputComboElement import InputComboElement
from GUI.Elements.InputIPElement import InputIPElement
from GUI.Elements.InputTextElement import InputTextElement
from Services.Main.Utils import validate_ip, check_is_port, call_error_dialog


class PowerForm:
    def __init__(self, context):
        self.__context = context
        self.__init_widget()

    def __init_widget(self):
        button = ButtonElement(self.__context).widget
        items = range(1, 30)
        items = list(map(lambda x: str(x), items))
        self.power_widget = InputComboElement(self.__context, items).widget

        button.clicked.connect(self.save_data)

        form_widget = FormElement(self.__context, 'Настройки мощности')
        form_widget.add_row('Мощность (dBm):', self.power_widget)
        form_widget.add_button(button)

        self.__box_widget = form_widget.widget

    def save_data(self):
        power = self.power_widget.currentText()
        print(power)

        self.__context.save_data({'power_dbi': power})

    @property
    def widget(self):
        return self.__box_widget



