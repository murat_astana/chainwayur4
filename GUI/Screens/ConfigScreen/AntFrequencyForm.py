from PyQt5.QtWidgets import QMessageBox

from GUI.Elements.ButtonElement import ButtonElement
from GUI.Elements.FormElement import FormElement
from GUI.Elements.InputComboElement import InputComboElement
from GUI.Elements.InputFrequencyElement import InputFrequencyElement
from GUI.Elements.InputIPElement import InputIPElement
from GUI.Elements.InputTextElement import InputTextElement
from Services.Main.Utils import validate_ip, check_is_port, call_error_dialog, check_is_frequency


class AntFrequencyForm:
    def __init__(self, context, ant_num: str):
        self.__context = context
        self.__ant_num = ant_num
        self.__init_widget()

    def __init_widget(self):
        button = ButtonElement(self.__context).widget

        self.frequency_widget = InputFrequencyElement(self.__context).widget

        button.clicked.connect(self.save_data)

        form_widget = FormElement(self.__context, 'Настройка ' + self.__ant_num + ' антены')
        form_widget.add_row('Частота (ms):', self.frequency_widget)
        form_widget.add_button(button)

        self.__box_widget = form_widget.widget

    def save_data(self):
        frequency = self.frequency_widget.text()
        if not frequency.isdigit() or not check_is_frequency(frequency):
            call_error_dialog('Поле "Частота". Укажите корректное значение')
            return

        self.__context.save_data({'ant_frequency_' + self.__ant_num: frequency})

    @property
    def widget(self):
        return self.__box_widget



