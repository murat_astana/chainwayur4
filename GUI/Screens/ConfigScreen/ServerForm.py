from PyQt5.QtWidgets import QMessageBox

from GUI.Elements.ButtonElement import ButtonElement
from GUI.Elements.FormElement import FormElement
from GUI.Elements.InputIPElement import InputIPElement
from GUI.Elements.InputTextElement import InputTextElement
from Services.Main.Utils import validate_ip, check_is_port, call_error_dialog
from Services.ServerRequest.ServerRequestService import ServerRequestService


class ServerForm:
    def __init__(self, context, server_api: ServerRequestService):
        self.__context = context
        self.__server_api = server_api
        self.__init_widget()

    def __init_widget(self):
        button = ButtonElement(self.__context).widget
        self.server_ip_widget = InputTextElement(self.__context).widget
        self.server_token_widget = InputTextElement(self.__context).widget

        button.clicked.connect(self.save_data)

        form_widget = FormElement(self.__context, 'Настройки сервера')
        form_widget.add_row('IP:', self.server_ip_widget)
        form_widget.add_row('Токен:', self.server_token_widget)
        form_widget.add_button(button)

        self.__box_widget = form_widget.widget

    def save_data(self):
        ip = self.server_ip_widget.text()
        token = self.server_token_widget.text()
        if not ip:
            call_error_dialog('Поле "IP". Неверный ip адрес. Укажите в правильном формате')
            return
        if not token:
            call_error_dialog('Поле "Токен". Пустое поле. Укажите правильное значение')
            return

        if not self.__server_api.ping(ip, token):
            call_error_dialog('Неверные данные')
            return

        self.__server_api.set_ip_and_token(ip, token)
        self.__context.save_data({'server_ip': ip, 'server_token': token})

    @property
    def widget(self):
        return self.__box_widget



