from functools import partial

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow, QGridLayout, QWidget, QTableWidget, QTableWidgetItem, \
    QHBoxLayout, QVBoxLayout, QLayout, QPushButton, QDesktopWidget, QTabWidget, QLabel
from PyQt5.QtCore import QSize, Qt

from GUI.Screens.RFIDScreen.RFIDScreen import RFIDScreen


class MainLayout(QMainWindow):
    def __init__(self, screens: list):
        QMainWindow.__init__(self)
        self.tab_index = None
        self.screens = screens

    def show(self) -> None:
        super().show()

        self.setWindowTitle('Приложение PrestoAgent')
        self.setWindowIcon(QtGui.QIcon('icon.ico'))

        self.setMinimumSize(QSize(800, 600))

        self.__init_left_layout()
        self.__init_right_layout()
        self.__init_main_layout()

    def __init_main_layout(self):
        main_layout = QHBoxLayout()
        main_layout.addWidget(self.__left_widget)
        main_layout.addWidget(self.__right_widget)
        main_layout.setStretch(0, 40)
        main_layout.setStretch(1, 200)
        main_widget = QWidget()
        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)

    def __init_left_layout(self):
        left_layout = QVBoxLayout()

        for index, obj in enumerate(self.screens):
            btn = QPushButton(obj['name'], self)
            btn.clicked.connect(partial(self.change_tab, index))
            left_layout.addWidget(btn)

        left_layout.addStretch(10)
        left_layout.setSpacing(15)

        left_layout.addWidget(QLabel('@2021-2022. TOO "Presto". Все права защищены'))

        self.__left_widget = QWidget()
        self.__left_widget.setLayout(left_layout)

    def __init_right_layout(self):
        self.__right_widget = QTabWidget()
        self.__right_widget.tabBar().setObjectName("mainTab")

        for _, obj in enumerate(self.screens):
            self.__right_widget.addTab(obj['widget'], '')

        self.__right_widget.setStyleSheet('''
            QTabBar::tab{
                width: 0; 
                height: 0; 
                margin: 0; 
                padding: 0; 
                border: none;
            }
        ''')


        self.__right_widget.setCurrentIndex(0)

    def change_tab(self, index):
        self.tab_index = index

        currentIndex = self.__right_widget.currentIndex()
        old_widget = self.screens[currentIndex]['widget']
        if hasattr(old_widget, 'leave_screen') and callable(getattr(old_widget, 'leave_screen')):
            old_widget.leave_screen()

        self.__right_widget.setCurrentIndex(self.tab_index)
        widget = self.screens[index]['widget']
        if hasattr(widget, 'load_data') and callable(getattr(widget, 'load_data')):
            widget.load_data()
