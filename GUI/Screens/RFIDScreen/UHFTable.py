from GUI.Elements.ShowTableElement import ShowTableElement


class UHFTable:
    def __init__(self, context):
        self.__context = context
        self.__widget = None
        self.__tags_row = {}
        self.__init_widget()

    def __init_widget(self):
        self.__widget = ShowTableElement(self.__context, ["EPC", "TID", "RSSI", "ANT", "Кол-во"])
        self.__widget.set_stretch_for_column(0)
        self.__widget.set_stretch_for_column(1)
        self.__widget.set_stretch_for_column(2)
        self.__widget.set_stretch_for_column(3)
        self.__widget.set_stretch_for_column(4)

    def add_tag(self, tag: dict):
        key = tag['epc'] + tag['tid']
        if key in self.__tags_row:
            row_num = self.__tags_row[key]
            tag_count = self.__widget.get_cell(row_num, 4)
            tag_count = int(tag_count) + 1

            self.__set_tag(row_num, tag, tag_count)
            return

        row_num = len(self.__tags_row.keys())
        self.__tags_row[key] = row_num
        self.__set_tag(row_num, tag, 1)

    def __set_tag(self, row_num: int, tag: dict, tag_count: int):
        self.__widget.set_cell(row_num, 0, tag['epc'])
        self.__widget.set_cell(row_num, 1, tag['tid'])
        self.__widget.set_cell(row_num, 2, str(tag['rssi_data']))
        self.__widget.set_cell(row_num, 3, str(tag['ant_data']))
        self.__widget.set_cell(row_num, 4, str(tag_count))

    @property
    def widget(self):
        return self.__widget.widget
