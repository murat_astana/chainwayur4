from PyQt5 import QtCore
from PyQt5.QtWidgets import QGroupBox, QGridLayout, QLabel
import time
from GUI.Elements.ShowFieldElement import ShowFieldElement


class CounterForm:
    def __init__(self, context):
        self.__context = context
        self.__box_widget = None
        self.__init_widget()
        self.__tags_row = {}
        self.__count_tag = 0
        self.__count_read = 0
        self.init_second = 0
        self.total_work_second = 0

    def start_time(self):
        self.init_second = int(time.time())
        print(self.init_second)

    def update_time(self):
        curr = int(time.time())
        if curr == self.total_work_second:
            return

        self.total_work_second = self.total_work_second + (curr - self.init_second)
        self.__total_time_widget.setText(time.strftime('%H:%M:%S', time.gmtime(self.total_work_second)))
        self.init_second = curr

    def __init_widget(self):
        self.__count_tag_widget = ShowFieldElement(self, '0').widget
        self.__count_read_widget = ShowFieldElement(self, '0').widget
        self.__total_time_widget = ShowFieldElement(self, '0').widget

        self.__box_widget = QGroupBox()

        label1 = QLabel('Количество считанных меток')
        label1.setAlignment(QtCore.Qt.AlignCenter)
        label2 = QLabel('Количество считываний')
        label2.setAlignment(QtCore.Qt.AlignCenter)
        label3 = QLabel('Время работы')
        label3.setAlignment(QtCore.Qt.AlignCenter)

        layout = QGridLayout()
        layout.setRowStretch(0 | 1 | 2 | 3, 50)
        layout.addWidget(label1, 0, 0)
        layout.addWidget(label2, 0, 1)
        layout.addWidget(label3, 0, 2)
        layout.addWidget(self.__count_tag_widget, 1, 0)
        layout.addWidget(self.__count_read_widget, 1, 1)
        layout.addWidget(self.__total_time_widget, 1, 2)

        self.__box_widget.setLayout(layout)

    def new_tag(self, key_tag: str):
        self.__count_read = self.__count_read + 1
        self.__count_read_widget.setText(str(self.__count_read))
        if key_tag in self.__tags_row:
            return

        self.__count_tag = self.__count_tag + 1
        self.__count_tag_widget.setText(str(self.__count_tag))
        self.__tags_row[key_tag] = True

    @property
    def widget(self):
        return self.__box_widget
