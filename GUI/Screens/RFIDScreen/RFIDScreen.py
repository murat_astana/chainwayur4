from PyQt5.QtWidgets import QGridLayout, QWidget, QLabel
from threading import Thread, Lock
import time
from DB.DBService import DBService
from GUI.Elements.ButtonElement import ButtonElement
from GUI.Elements.ShowTableElement import ShowTableElement
from GUI.Screens.RFIDScreen.CounterForm import CounterForm
from GUI.Screens.RFIDScreen.UHFTable import UHFTable
from GUI.Screens.RFIDScreen.UhfThread import UhfThread
from Services.RFID.RFIDCommander import RFIDCommander, CommandNameType
from Services.SaveTagToDBService import SaveTagToDBService
from Services.ServerRequest.ServerRequestService import ServerRequestService


class RFIDScreen(QWidget):
    __main_layout = None

    def __init__(self, uhf: RFIDCommander, db: DBService, server_api: ServerRequestService):
        super(self.__class__, self).__init__()
        self.__uhf = uhf
        self.__db = db
        self.__table = UHFTable(self)
        self.__counter = CounterForm(self)
        self.__button_is_active = False
        self.__save_tag_to_db_service = SaveTagToDBService(self.__db)
        self.__server_api = server_api
        self.__init_layout()

    def call_uhf(self, tag):
        self.__table.add_tag(tag)
        tag_key = tag['epc'] + tag['tid']
        self.__counter.new_tag(tag_key)
        self.__save_tag_to_db_service.add(tag)
        if self.__save_tag_to_db_service.tag_list_is_fill():
            epc_list = self.__save_tag_to_db_service.get_tag_list_and_clear()
            self.__server_api.save_epc_list(epc_list)


    def __init_layout(self):
        self.__button = ButtonElement(self, 'Включить поиск').widget
        self.__button.clicked.connect(self.__btn_click)

        self.__main_layout = QGridLayout()

        self.__main_layout.addWidget(self.__counter.widget, 0, 0)
        self.__main_layout.addWidget(self.__button, 1, 0)
        self.__main_layout.addWidget(self.__table.widget, 2, 0)
        self.__main_layout.setRowStretch(0, 1)
        self.__main_layout.setRowStretch(1, 1)
        self.__main_layout.setRowStretch(2, 50)

        self.setLayout(self.__main_layout)

    def __btn_click(self):
        if self.__button_is_active:
            self.__button_is_active = False
            self.__button.setText('Включить поиск')
            self.__search_close()
            return

        self.__button.setText('Отключить поиск')
        self.__search_open()
        self.__button_is_active = True

    def __search_open(self):
        print('__search_open')
        self.__counter.start_time()
        self.__uhf_thread = UhfThread()
        self.__uhf_thread.add_uhf(self.__uhf)
        self.__uhf_thread.uhf_signal.connect(self.call_uhf)
        self.__uhf_thread.tick_signal.connect(self.tick)
        self.__uhf.call_command(CommandNameType.InventoryStart)
        self.__uhf_thread.start()


    def tick(self):
        self.__counter.update_time()

    def __search_close(self):
        print('__search_close')
        self.__uhf_thread.stop()
        self.__uhf.call_command(CommandNameType.InventoryStop)

        epc_list = self.__save_tag_to_db_service.get_tag_list_and_clear()
        if len(epc_list) > 0:
            self.__server_api.save_epc_list(epc_list)

    def load_data(self):
        pass

    def leave_screen(self):
        if self.__button_is_active:
            self.__btn_click()

        print('leave_screen from rfid')
