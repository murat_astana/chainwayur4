from PyQt5.QtCore import QThread, pyqtSignal

from Services.RFID.RFIDCommander import CommandNameType
import time


class UhfThread(QThread):
    uhf_signal = pyqtSignal(dict)
    tick_signal = pyqtSignal()

    def __init__(self, parent=None):
        super(UhfThread, self).__init__(parent)
        self.__uhf = None
        self.threadactive = True
        self.total = 100

    def add_uhf(self, uhf):
        self.__uhf = uhf

    def run(self):
        while self.threadactive:
            tag_res = self.__uhf.call_command(CommandNameType.GetTagWhenInventory)
            if tag_res:
                self.uhf_signal.emit(tag_res)
            time.sleep(0.05)
            self.tick_signal.emit()

    def stop(self):
        self.threadactive = False
        self.wait()
