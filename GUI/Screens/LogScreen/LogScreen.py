from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QGridLayout

from App import logger
from GUI.Elements.ShowTableElement import ShowTableElement


class LogScreen(QWidget):
    __main_layout = None
    __table = None
    __uhf = None

    def __init__(self, uhf):
        super(LogScreen, self).__init__()
        self.__init_layout()
        self.__uhf = uhf
        self.__logger = logger()
        self.__loaded_data = False

    def __init_layout(self):
        self.__main_layout = QGridLayout()

        self.__table = ShowTableElement(self, ["Время", "Тип", "Сообшение"])
        self.__table.set_stretch_for_column(2)
        self.__main_layout.addWidget(self.__table.widget, 0, 0)

        self.setLayout(self.__main_layout)

    def load_data(self):
        if self.__loaded_data:
            return

        self.__counter_row = 0
        self.__data = self.__get_logger_data()
        for conf in self.__data:
            self.__table.set_cell(self.__counter_row, 0, conf['date'])
            self.__table.set_cell(self.__counter_row, 1, conf['type'])
            self.__table.set_cell(self.__counter_row, 2, conf['message'])
            self.__counter_row = self.__counter_row + 1

        self.__loaded_data = True

    def __get_logger_data(self):
        lines = self.__logger.get_data()
        return lines

