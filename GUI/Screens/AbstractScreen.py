from PyQt5.QtWidgets import QWidget
from abc import ABC, abstractmethod


class AbstractScreen(QWidget):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.init_layout()

    @abstractmethod
    def init_layout(self):
        pass
