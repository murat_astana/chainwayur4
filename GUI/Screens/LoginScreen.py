import ipaddress
import os

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QMainWindow, QHBoxLayout, QWidget

from DB.DBService import DBService
from GUI.Elements.ButtonElement import ButtonElement
from GUI.Elements.FormElement import FormElement
from GUI.Elements.InputIPElement import InputIPElement
from GUI.Elements.InputPortElement import InputPortElement
from GUI.Elements.InputTextElement import InputTextElement
from Services.Main.Utils import validate_ip, call_error_dialog, check_is_port, call_info_dialog
from Services.RFID.RFIDCommander import RFIDCommander


class LoginScreen(QMainWindow):
    switch_window = QtCore.pyqtSignal()

    def __init__(self, db: DBService):
        QMainWindow.__init__(self)
        self.uhf = None
        self.__db = db
        self.__config_data = self.__db.config.get_all()

    def show(self) -> None:
        super().show()
        self.setWindowTitle('Приложение PrestoAgent. Форма входа')
        self.setWindowIcon(QtGui.QIcon('icon.ico'))

        self.setMinimumSize(QSize(400, 200))
        self.setMinimumSize(QSize(400, 200))

        self.__init_main_layout()

    def __init_form_widget(self):
        button = ButtonElement(self, 'Войти').widget
        self.ip_widget = InputIPElement(self).widget
        self.port_widget = InputPortElement(self).widget

        button.clicked.connect(self.login)

        form_widget = FormElement(self, 'Форма входа')
        form_widget.add_row('IP:', self.ip_widget)
        form_widget.add_row('Порт:', self.port_widget)
        form_widget.add_button(button)

        if self.__config_data['device_ip'] is not None and self.__config_data['device_port'] is not None:
            self.ip_widget.setText(self.__config_data['device_ip'])
            self.port_widget.setText(str(self.__config_data['device_port']))

        self.__form_widget = form_widget.widget

    def __init_main_layout(self):
        self.__init_form_widget()

        main_layout = QHBoxLayout()
        main_layout.addWidget(self.__form_widget)

        main_widget = QWidget()
        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)

    def login(self):
        ip = self.ip_widget.text()
        port = self.port_widget.text()
        if not validate_ip(ip):
            call_error_dialog('Поле "IP". Неверный ip адрес. Укажите в правильном формате')
            return
        if not port.isdigit() or not check_is_port(port):
            call_error_dialog('Поле "Порт". Неверное поле или пустое. Укажите правильное значение')
            return

        self.__db.config.update('device_ip', ip)
        self.__db.config.update('device_port', int(port))
        self.__config_data = self.__db.config.get_all()

        try:
            uhf = RFIDCommander(ip, int(port))
        except:
            call_error_dialog('Не удалось подключиться к устройству')
            return

        call_info_dialog('Соединение успешно установлено')
        self.uhf = uhf
        self.switch_window.emit()
