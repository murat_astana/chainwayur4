from DB.DBService import DBService
from GUI.Screens.ConfigScreen.ConfigScreen import ConfigScreen
from GUI.Screens.InfoScreen.InfoScreen import InfoScreen
from GUI.Screens.LogScreen.LogScreen import LogScreen
from GUI.Screens.MainLayout import MainLayout
from GUI.Screens.RFIDScreen.RFIDScreen import RFIDScreen
from Services.RFID.RFIDCommander import RFIDCommander
from Services.ServerRequest.ServerRequestService import ServerRequestService


class MainController:
    def __init__(self, uhf: RFIDCommander, db: DBService):
        self.__main_layout = None
        self.__uhf = uhf
        self.__db = db
        self.__init_server_api()
        self.__screens = [
            {
                'widget': RFIDScreen(uhf, db, self.__server_api),
                'name': 'Главная'
            },
            {
                'widget': ConfigScreen(uhf, db, self.__server_api),
                'name': 'Настройки'
            },
            {
                'widget': LogScreen(uhf),
                'name': 'Логи'
            },
            {
                'widget': InfoScreen(uhf, self.__server_api),
                'name': 'Инфо'
            }
        ]

    def __init_server_api(self):
        config = self.__db.config.get_all()
        self.__server_api = ServerRequestService(config['server_ip'], config['server_token'])

    def show(self):
        print('Runned GUI')
        self.__main_layout = MainLayout(self.__screens)
        self.__main_layout.show()


