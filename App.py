import os
from Services.Main.Logger import Logger

__logger = Logger()


def logger():
    return __logger


def config(attr: str, default=None):
    val = os.getenv(attr)
    if val is None:
        val = default

    return val
