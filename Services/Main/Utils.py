from PyQt5.QtWidgets import QMessageBox


def print_res(response, command_name):
    if not response:
        print('cant do command' + command_name)
    elif isinstance(response, str):
        print('success do command' + command_name + " . res is " + response)
    elif isinstance(response, int):
        print('success do command' + command_name + " . res is " + str(response))
    else:
        print('success do command' + command_name)


def validate_ip(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True


def check_int(s):
    if s[0] in ('-', '+'):
        return s[1:].isdigit()
    return s.isdigit()


def check_is_port(s):
    if not check_int(s):
        return False

    s = int(s)
    if s < 0 or s > 9999:
        return False

    return True


def check_is_frequency(s):
    if not check_int(s):
        return False

    s = int(s)
    if s < 10 or s > 65535:
        return False

    return True


def call_error_dialog(text: str, title: str = 'Ошибка'):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(text)
    msg.setWindowTitle(title)
    msg.exec_()


def call_info_dialog(text: str, title: str = 'Успешно'):
    msg = QMessageBox()
    msg.setText(text)
    msg.setWindowTitle(title)
    msg.exec_()
