import logging as logger_def

logger_def.basicConfig(filename='app.log', filemode='a', format='%(asctime)s || %(name)s || %(levelname)s || %(message)s ')


class Logger:
    def __init__(self):
        self.__logger = logger_def

    def warning(self, message: str):
        self.__logger.warning(message)

    def info(self, message: str):
        self.__logger.warning(message)

    def debug(self, message: str):
        self.__logger.debug(message)

    def critical(self, message: str):
        self.__logger.critical(message)

    def get_data(self):
        file = open('app.log', 'r')
        lines = file.read().splitlines()
        lines.sort(reverse=True)
        file.close()

        res = []
        for l in lines:
            if not l:
                continue

            data = l.split("||")
            if len(data) != 4:
                continue

            res.append({
                'date': data[0].strip(),
                'type': data[2].strip(),
                'message': data[3].strip()
            })

        return res
