
def parse_urf_data (uhf_data: list, uhf_len: int):
    str_uhf = __bit_to_string(uhf_data, 0, uhf_len)

    uii_len: int = uhf_data[0]
    tid_len: int = uhf_data[uii_len + 1]
    tid_index: int = uii_len + 2
    rssi_index: int = 1 + uii_len + 1 + tid_len
    ant_index: int = rssi_index + 2

    epc = str_uhf[6:6 + (uii_len * 2 - 4)]
    tid = str_uhf[tid_index * 2:(tid_index * 2) + (tid_len * 2)]

    rssi_data = str_uhf[rssi_index * 2: (rssi_index * 2) + 4]
    rssi_data = int(rssi_data, 16) - 65535
    rssi_data = (rssi_data / 10.0)

    ant_data = str_uhf[ant_index * 2: (ant_index * 2) + 2]
    ant_data = int(ant_data, 16)

    return {
        'epc': epc,
        'tid': tid,
        'rssi_data': rssi_data,
        'ant_data': ant_data
    }



def __hex_value( i: int):
    if i < 10:
        i = i + 48
        return chr(i)
    i = i - 10 + 65
    return chr(i)


def __bit_to_string(ar_byte, startIndex: int, length: int):
    num1: int = len(ar_byte)  # нужно узнать как его обрабатывать
    num2: int = length
    if num2 == 0:
        return 0

    chArray = []
    num = 0
    end_list = num2 * 3
    while num < end_list:
        chArray.append(0)
        num = num + 1

    num3: int = startIndex
    index: int = 0

    while index < end_list:
        num4 = ar_byte[num3]
        index_1 = index
        index_2 = index + 1
        index_3 = index + 2

        arg_1 = int(num4 / 16)
        arg_2 = int(num4 % 16)

        chArray[index_1] = __hex_value(arg_1)
        chArray[index_2] = __hex_value(arg_2)
        chArray[index_3] = ''
        num3 = num3 + 1
        index = index + 3

    return "".join(chArray)
