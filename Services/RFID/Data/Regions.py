__regions = {
    0x01: 'China1',
    0x02: 'China2',
    0x04: 'Europe',
    0x08: 'USA',
    0x16: 'Korea',
    0x32: 'Japan',
}


def get_region_map():
    m = {}
    for key in __regions:
        m[key.real] = __regions[key.real]
    return m


def find_region_by_int(region_key: int):
    if region_key not in __regions:
        return None

    return __regions[region_key]


def find_int_by_region(region: str):
    int_id: int = 0

    for key in __regions:
        if region == __regions[key]:
            int_id = key.real

    return int_id


def get_region_list_in_str():
    return ", ".join(__regions.values())
