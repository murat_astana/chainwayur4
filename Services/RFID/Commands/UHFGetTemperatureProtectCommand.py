from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class UHFGetTemperatureProtectCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFGetTemperatureProtect')

    def call(self):
        v: str = ''
        self.__comm.add_arg(v)
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        print(res[0].value.decode())
        return res[0].value
