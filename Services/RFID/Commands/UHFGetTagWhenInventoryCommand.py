import ctypes

from Services.Main.ParseUhfData import parse_urf_data

from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class UHFGetTagWhenInventoryCommand(AbstractCommand):
    def __init__(self, dll):
        self._dll = dll
        self.__comm = CommandHandler(dll, 'UHF_GetReceived_EX')

    def call(self):
        tag: int = 150
        byte = (ctypes.c_ubyte * 500)(100, 200)
        self.__comm.add_arg(tag, True)
        self.__comm.add_byte_arg(byte)
        self.__comm.handle()

        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        uhf = parse_urf_data(res[1], res[0].value)

        return uhf

