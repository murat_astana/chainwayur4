from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class UHFInventoryStartCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFInventory')

    def call(self):
        self.__comm.handle()
        print('!!!!!!!!!!!!!!!inventory started !!!!!!!!!!!!!!!')
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        return True
