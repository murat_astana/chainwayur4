from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class TCPDisconnectCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'TCPDisconnect')

    def call(self):
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        return ''
