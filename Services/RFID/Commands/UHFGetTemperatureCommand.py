from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class UHFGetTemperatureCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFGetTemperature')

    def call(self):
        temp: int = 500
        self.__comm.add_arg(temp, True)
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        return res[0].value
