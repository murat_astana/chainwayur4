import binascii
import ctypes
from ctypes import c_int, c_char_p, c_int32, c_uint, POINTER, c_byte, c_char, c_double

from Services.RFID.Exceptions.MissingCommandNameException import MissingCommandNameException
from Services.RFID.Exceptions.OSErrorException import OSErrorException
from Services.RFID.Exceptions.WrongTypeException import WrongTypeException
from abc import ABC, abstractmethod


def _check_type_by_name(val_type):
    if val_type not in ['str', 'int']:
        raise WrongTypeException(val_type)


def _check_type_by_val(val):
    if not isinstance(val, str) and not isinstance(val, int):
        raise WrongTypeException(val)


def _get_c_type(arg):
    if isinstance(arg, str):
        return c_char_p

    return c_int


def _get_c_val(arg):
    if isinstance(arg, str):
        return c_char_p(arg.encode())

    return c_int(arg)


class MyStructure(ctypes.Structure):
    _fields_ = [("a", ctypes.c_char * 150)]


class CommandHandler:
    def __init__(self, dll, command_name):
        self.__dll = dll
        self.__response = False

        self.__res_type = c_int
        self.__arg_type_list = []
        self.__arg_list = []

        self.__command_name = command_name
        self.__command = False

    def set_res_type(self, res_type):
        _check_type_by_name(res_type)
        if res_type == 'str':
            self.__res_type = c_char_p
            return
        self.__res_type = c_int

    def clear_args(self):
        self.__arg_type_list = []
        self.__arg_list = []

    def add_byte_arg(self, byte):
        self.__arg_list.append(byte)

    def add_arg(self, arg, is_pointer: bool = False):
        _check_type_by_val(arg)

        if is_pointer:
            self.__arg_type_list.append(POINTER(_get_c_type(arg)))
            self.__arg_list.append(_get_c_val(arg))
            return

        self.__arg_type_list.append(_get_c_type(arg))
        self.__arg_list.append(_get_c_val(arg))

    def handle(self):
        self.__check_command()
        self.__command = getattr(self.__dll, self.__command_name)
        self.__command.argtypes = self.__arg_type_list
        self.__command.restype = self.__res_type

        try:
            if len(self.__arg_list) == 0:
                self.__response = self.__command()
            else:
                self.__response = self.__command(*self.__arg_list)
        except OSError as e:
            print(self.__arg_type_list)
            raise OSErrorException(str(e))
        except Exception as e:
            raise e

    def __check_command(self):
        if self.__command_name == '':
            raise MissingCommandNameException()

    @property
    def response(self):
        return self.__response

    @property
    def arguments(self):
        return self.__arg_list


class AbstractCommand(ABC):
    @abstractmethod
    def __init__(self, dll):
        pass

    @abstractmethod
    def call(self, *args):
        pass

    @property
    @abstractmethod
    def response(self):
        pass
