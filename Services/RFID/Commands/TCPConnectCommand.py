from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class TCPConnectCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'TCPConnect')

    def call(self, ip, port):
        self.__comm.add_arg(ip)
        self.__comm.add_arg(port)
        self.__comm.handle()

        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        return True