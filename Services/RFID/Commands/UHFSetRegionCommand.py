from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand
from Services.RFID.Data.Regions import find_int_by_region, get_region_list_in_str, find_region_by_int
from Services.RFID.Exceptions.IncorrectArgumentOfCommandException import IncorrectArgumentOfCommandException


class UHFSetRegionCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFSetRegion')

    def call(self, region: str):
        region_id = find_int_by_region(region)
        if region_id == 0:
            raise IncorrectArgumentOfCommandException(region_id, 'region must be on of values:' + get_region_list_in_str())

        res_set_power = 0

        self.__comm.add_arg(res_set_power, True)
        self.__comm.add_arg(region_id)
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        print("____________--")
        if res[0].value == 0:
            return find_region_by_int(res[1].value)

