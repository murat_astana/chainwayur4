from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class UHFGetDeviceIDCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFGetDeviceID')

    def call(self):
        version = ''
        self.__comm.add_arg(version)
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        return res[0].value.decode()
