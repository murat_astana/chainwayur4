from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand
from Services.RFID.Data.Regions import find_region_by_int


class UHFGetRegionCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFGetRegion')

    def call(self):
        v22 = 0
        self.__comm.add_arg(v22, True)
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        return find_region_by_int(res[0].value)
