from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class UHFInventoryStopCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFStopGet')

    def call(self):
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        return True
