from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand
from Services.RFID.Exceptions.IncorrectArgumentOfCommandException import IncorrectArgumentOfCommandException


class UHFSetANTWorkTimeCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFSetANTWorkTime')

    def call(self, ant_num: int, work_time: int):
        if ant_num < 1 or ant_num > 16:
            raise IncorrectArgumentOfCommandException(str(ant_num), 'this param should be more than 0 and less than 17')
        if work_time < 1 or work_time > 65535:
            raise IncorrectArgumentOfCommandException(str(work_time), 'this param should be more than 0 and less than '
                                                                      '65536')

        res_set_power = 0

        self.__comm.add_arg(ant_num)
        self.__comm.add_arg(res_set_power, True)
        self.__comm.add_arg(work_time)

        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        if res[1].value == 0:
            return res[2].value

        return False
