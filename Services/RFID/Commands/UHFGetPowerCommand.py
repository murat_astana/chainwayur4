from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand


class UHFGetPowerCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFGetPower')

    def call(self):
        v22 = 1
        self.__comm.add_arg(v22, True)
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        print(res[0].value)
        return res[0].value
