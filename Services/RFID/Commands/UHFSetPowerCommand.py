from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand
from Services.RFID.Exceptions.IncorrectArgumentOfCommandException import IncorrectArgumentOfCommandException


class UHFSetPowerCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFSetPower')

    def call(self, power: int):
        if power < 1 or power > 30:
            raise IncorrectArgumentOfCommandException(str(power), 'need more then 1 and less then 30');

        res_set_power = 0

        self.__comm.add_arg(res_set_power, True)
        self.__comm.add_arg(power)

        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        if res[0].value == 0:
            return res[1].value

        return False
