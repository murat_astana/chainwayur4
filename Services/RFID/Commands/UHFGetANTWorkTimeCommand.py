from Services.RFID.Commands.CommandHandler import CommandHandler, AbstractCommand
from Services.RFID.Exceptions.IncorrectArgumentOfCommandException import IncorrectArgumentOfCommandException


class UHFGetANTWorkTimeCommand(AbstractCommand):
    def __init__(self, dll):
        self.__comm = CommandHandler(dll, 'UHFGetANTWorkTime')

    def call(self, ant_num: int):
        if ant_num < 1 or ant_num > 16:
            raise IncorrectArgumentOfCommandException(str(ant_num), 'this param should be more than 0 and less than 17')

        work_time: str = 0
        self.__comm.add_arg(ant_num)
        self.__comm.add_arg(work_time, True)
        self.__comm.handle()
        if self.__comm.response != 0:
            return False

        return True

    @property
    def response(self):
        res = self.__comm.arguments
        return res[1].value
