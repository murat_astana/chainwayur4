from ctypes import *

from enum import Enum

from App import logger
from Services.RFID.Commands.GetHardwareVersionCommand import GetHardwareVersionCommand
from Services.RFID.Commands.TCPConnectCommand import TCPConnectCommand
from Services.RFID.Commands.TCPDisconnectCommand import TCPDisconnectCommand
from Services.RFID.Commands.UHFGetANTWorkTimeCommand import UHFGetANTWorkTimeCommand
from Services.RFID.Commands.UHFGetDeviceIDCommand import UHFGetDeviceIDCommand
from Services.RFID.Commands.UHFGetJumpFrequencyCommand import UHFGetJumpFrequencyCommand
from Services.RFID.Commands.UHFGetPowerCommand import UHFGetPowerCommand
from Services.RFID.Commands.UHFGetRegionCommand import UHFGetRegionCommand
from Services.RFID.Commands.UHFGetSoftwareCommand import UHFGetSoftwareCommand
from Services.RFID.Commands.UHFGetTagWhenInventoryCommand import UHFGetTagWhenInventoryCommand
from Services.RFID.Commands.UHFGetTemperatureCommand import UHFGetTemperatureCommand
from Services.RFID.Commands.UHFGetTemperatureProtectCommand import UHFGetTemperatureProtectCommand
from Services.RFID.Commands.UHFInventoryStartCommand import UHFInventoryStartCommand
from Services.RFID.Commands.UHFInventoryStopCommand import UHFInventoryStopCommand
from Services.RFID.Commands.UHFSetANTWorkTimeCommand import UHFSetANTWorkTimeCommand
from Services.RFID.Commands.UHFSetPowerCommand import UHFSetPowerCommand
from Services.RFID.Commands.UHFSetRegionCommand import UHFSetRegionCommand
from Services.RFID.Exceptions.CannotConnectedWithRfidException import CannotConnectedWithRfidException
from Services.RFID.Exceptions.CommandNotExistException import CommandNotExistException
from Services.RFID.Exceptions.WrongArgumentTypesException import WrongArgumentTypesException


class RFIDCommander:
    def __init__(self, ip, port):
        logger().info('Inited Rfid commander. IP - ' + ip + ' .Port - ' + str(port))

        self.__dll = CDLL(r"C:\PrestoAgent\UHFAPI.dll")
        self.__connect(ip, port)

    def __connect(self, ip, port):
        connect = TCPConnectCommand(self.__dll)
        self.__is_connected = connect.call(ip, port)
        if not self.__is_connected:
            raise CannotConnectedWithRfidException()

    def call_command(self, command_name, args=None):
        if args is None:
            args = []

        if not isinstance(command_name, CommandNameType):
            raise CommandNotExistException()

        if not isinstance(args, list):
            raise WrongArgumentTypesException()

        command = self.__get_command_object(command_name)
        if len(args) == 0:
            success = command.call()
        else:
            success = command.call(*args)

        if not success:
            return False

        return command.response

    def __get_command_object(self, command_name):
        if command_name == CommandNameType.GetHardwareVersion:
            return GetHardwareVersionCommand(self.__dll)
        if command_name == CommandNameType.Connect:
            return TCPConnectCommand(self.__dll)
        if command_name == CommandNameType.Disconnect:
            return TCPDisconnectCommand(self.__dll)
        if command_name == CommandNameType.GetSoftwareVersion:
            return UHFGetSoftwareCommand(self.__dll)
        if command_name == CommandNameType.GetDeviceID:
            return UHFGetDeviceIDCommand(self.__dll)
        if command_name == CommandNameType.GetTemperature:
            return UHFGetTemperatureCommand(self.__dll)
        if command_name == CommandNameType.GetTemperatureProtect:
            return UHFGetTemperatureProtectCommand(self.__dll)
        if command_name == CommandNameType.GetPower:
            return UHFGetPowerCommand(self.__dll)
        if command_name == CommandNameType.SetPower:
            return UHFSetPowerCommand(self.__dll)
        if command_name == CommandNameType.GetRegion:
            return UHFGetRegionCommand(self.__dll)
        if command_name == CommandNameType.SetRegion:
            return UHFSetRegionCommand(self.__dll)
        if command_name == CommandNameType.GetJumpFrequency:
            return UHFGetJumpFrequencyCommand(self.__dll)
        if command_name == CommandNameType.GetANTWorkTime:
            return UHFGetANTWorkTimeCommand(self.__dll)
        if command_name == CommandNameType.SetANTWorkTime:
            return UHFSetANTWorkTimeCommand(self.__dll)
        if command_name == CommandNameType.InventoryStart:
            return UHFInventoryStartCommand(self.__dll)
        if command_name == CommandNameType.InventoryStop:
            return UHFInventoryStopCommand(self.__dll)
        if command_name == CommandNameType.GetTagWhenInventory:
            return UHFGetTagWhenInventoryCommand(self.__dll)






class CommandNameType(Enum):
    GetHardwareVersion = 'GetHardwareVersion'
    Connect = 'TCPConnectCommand'
    Disconnect = 'TCPDisconnectCommand'
    GetSoftwareVersion = 'UHFGetSoftwareVersion'
    GetDeviceID = 'UHFGetDeviceID'
    GetTemperature = 'GetTemperature'
    GetTemperatureProtect = 'GetTemperatureProtect'
    GetPower = 'UHFGetPowerCommand'
    SetPower = 'UHFSetPowerCommand'
    GetRegion = 'UHFGetRegionCommand'
    SetRegion = 'UHFSetRegionCommand'
    GetJumpFrequency = 'UHFGetJumpFrequencyCommand'
    GetANTWorkTime = 'UHFGetANTWorkTimeCommand'
    SetANTWorkTime = 'UHFSetANTWorkTimeCommand'
    InventoryStart = 'UHFInventoryStartCommand'
    InventoryStop = 'UHFInventoryStopCommand'
    GetTagWhenInventory = 'GetTagWhenInventory'
