class OSErrorException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return F" Os error: {self.message}"