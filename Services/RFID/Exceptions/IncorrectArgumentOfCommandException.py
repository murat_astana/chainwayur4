class IncorrectArgumentOfCommandException(Exception):
    def __init__(self, arg: str, need_rule:str):
        self.arg = arg
        self.need_rule = need_rule

    def __str__(self):
        return F" Incorrect argument of command exception. Called argument {self.arg}, need to be {self.need_rule}"
