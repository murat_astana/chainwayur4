class WrongTypeException(Exception):
    def __init__(self, type):
        self.type = type

    def __str__(self):
        return F" Wrong type {self.type}"