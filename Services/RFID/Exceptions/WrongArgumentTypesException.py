
class WrongArgumentTypesException(Exception):

    def __str__(self):
        return F" wrong argument types"
