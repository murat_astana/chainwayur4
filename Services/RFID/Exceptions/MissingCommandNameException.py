class MissingCommandNameException(Exception):

    def __str__(self):
        return F" Missing command name"
