import requests

from DB.Model import ConfigModel
from Services.Main.Utils import validate_ip
from Services.ServerRequest.SendRequestThread import SendRequestThread


class ServerRequestService:
    def __init__(self, ip: str, token: str):
        self.__ip = ip
        self.__token = token

    def __get(self, path: str, data: dict = None):
        my_headers = {'Authorization': 'Bearer ' + self.__token}
        response = requests.get(self.__ip + path, headers=my_headers)
        return response

    def __post(self, path: str, data = None):
        if not validate_ip(self.__ip):
            return True

        self.__thread = SendRequestThread()
        self.__thread.add_data('post', self.__ip + path, self.__token, data)
        self.__thread.start()

        return True

    def set_ip_and_token(self, ip: str, token: str):
        self.__ip = ip
        self.__token = token

    def ping(self, ip: str, token: str):
        my_headers = {'Authorization': 'Bearer ' + token}
        response = requests.get(ip + '/ping', headers=my_headers)

        if response.status_code != 200:
            return False

        return True

    def save_config(self, data: dict):
        r = self.__post('configs', data)
        return True

    def save_info(self, data: dict):
        r = self.__post('device-info', data)
        return True

    def save_epc_list(self, epc_list: list):
        r = self.__post('epc', {'data': epc_list})
        return True

