import requests
from PyQt5.QtCore import QThread, pyqtSignal


class SendRequestThread(QThread):
    res_signal = pyqtSignal(object)

    def __init__(self, parent=None):
        super(SendRequestThread, self).__init__(parent)
        self.__path = None
        self.__data = None
        self.__r_token = None
        self.__r_type = None

    def add_data(self, r_type: str, path: str, r_token: str, data = None):
        self.__r_type = r_type
        self.__r_token = r_token
        self.__data = data
        self.__path = path

    def run(self):
        my_headers = {'Authorization': 'Bearer ' + self.__r_token}
        if self.__r_type == 'get':
            response = requests.get(self.__path, headers=my_headers)
        elif self.__data is not None:
            response = requests.post(self.__path, headers=my_headers, data=self.__data)
        else:
            response = requests.post(self.__path, headers=my_headers)

        self.res_signal.emit(response)
