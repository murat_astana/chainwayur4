from DB.DBService import DBService
import time


class SaveTagToDBService:
    def __init__(self, db: DBService):
        self.__db = db
        self.__tags = {}
        self.__tag_list = []

    def add(self, tag: dict):
        tag_key = tag['epc'] + tag['tid'] + str(tag['ant_data'])

        if tag_key not in self.__tags:
            self.__tags[tag_key] = int(time.time())
            self.__save_tag(tag, True)
            return

        if (int(time.time()) - self.__tags[tag_key]) < 30:
            return

        self.__save_tag(tag, True)

        self.__tags[tag_key] = int(time.time())

    def __save_tag(self, tag: dict, is_need_add_to_list: bool):
        tag = self.__db.uhf_tag.add(tag['epc'], tag['tid'], tag['rssi_data'], tag['ant_data'])
        if is_need_add_to_list:
            self.__tag_list.append(tag)

    def get_tag_list_and_clear(self):
        tags_list = self.__tag_list
        self.__tag_list = []

        return tags_list

    def tag_list_is_fill(self):
        if len(self.__tag_list) < 5:
            return False

        return True

